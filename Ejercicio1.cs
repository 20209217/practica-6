//1.Crear una clase Persona que tenga como atributos el "cedula, nombre, apellido y la edad (definir las propiedades 
//para poder acceder a dichos atributos)". Definir como responsabilidad un método para mostrar ó imprimir. 
//Crear una segunda clase Profesor que herede de la clase Persona. Añadir un atributo sueldo ( y su propiedad) y el 
//método para imprimir su sueldo. Definir un objeto de la clase Persona y llamar a sus métodos y propiedades. 
//También crear un objeto de la clase Profesor y llamar a sus métodos y propiedades//


namespace Práctica_6.Constructores_y_Herencia
    {

using System;

class Persona
{

    public string Cedula;
    public string Nombre;

    public string Apellido;

    public int Edad;

    void Cumpleaños() 
    {
        Edad++;
    }

    public Persona(string nombre, int edad, string cedula, string apellido)
    {
        Cedula = cedula;
        Apellido = apellido;
        Nombre = nombre;
        Edad = edad;
    }
}

class Profesor : Persona
{

    public int Sueldo;

    Profesor(string nombre, int edad, string cedula, int sueldo, string apellido)
       :base(nombre, edad, apellido, cedula)
    {  
        Sueldo = sueldo;
    }

    public static void Main()
    {
        Profesor p = new Profesor ("Jesús", 22, "Fernández", 18000, "001-1896742-0");
        Console.WriteLine("Cedula=" + p.Cedula);
        Console.WriteLine("Nombre=" + p.Nombre);
        Console.WriteLine("Apellido=" + p.Apellido);
        Console.WriteLine("Edad=" + p.Edad);
        Console.WriteLine("Sueldo=" + p.Sueldo);
    }
}
    }
