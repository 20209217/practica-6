//2.Crear una clase Contacto. Esta clase deberá tener los atributos 
//"nombre, apellidos, telefono y direccion". También deberá tener 
// un método "SetContacto", de tipo void y con los parámetros string, 
// que permita cambiar el valor de los atributos. 
// También tendrá un método "Saludar", que escribirá 
// en pantalla "Hola, soy " seguido de sus datos. 
// Crear también una clase llamada ProbarContacto. 
// Esta clase deberá contener sólo la función Main, que creará dos objetos 
// de tipo Contacto, les asignará los datos del contacto y les pedirá que saluden.

using System;

namespace Práctica_6.ejercicio2
{
  class Contacto
  {
      // string nombre;
      // string apellido;
      // string telefono;
      // string direccion;
    public virtual void SetContacto()
    {
      Console.Write("Maria" + " " + "Del Carmen" + " " + "809-535-0111" + " " + "Avenida Mella #19");
    }
  }

  class Saludo : Contacto
{
  public override void SetContacto() 
  {
    Console.Write("Hola, Soy ");
  }
}

  class Program
  {
    static void Main(string[] args)
    {
      Contacto mycontacto = new Contacto();
      Saludo mySaludo = new Saludo(); 
      
      mySaludo.SetContacto();
      mycontacto.SetContacto();
      
    }
  }
}
