//3.Crear tres clases ClaseA, ClaseB, ClaseC que ClaseB herede de ClaseA y ClaseC herede de ClaseB. 
//Definir un constructor a cada clase que muestre un mensaje. Luego definir un objeto de la clase ClaseC.

using System;

namespace Práctica_6.Ejercicio3
{
class a  //
{
  public virtual void tipoclases() 
  {
    Console.WriteLine("Bienvenido a la Clase A");
  }
}

class b : a  //
{
  public override void tipoclases() 
  {
    Console.WriteLine("Bienvenido a la Clase B");
  }
}

class c : b 
{
  public override void tipoclases() 
  {
    Console.WriteLine("Bienvenido a la Clase C");
  }
}

class Program 
{
  static void Main(string[] args) 
  {
    a myclassa = new a(); 
    b myclassb = new b(); 
    c myclassc = new c();  

    myclassa.tipoclases();
    myclassb.tipoclases();
    myclassc.tipoclases();
  }
}
}
